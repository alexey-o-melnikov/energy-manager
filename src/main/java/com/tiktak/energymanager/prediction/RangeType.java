package com.tiktak.energymanager.prediction;

/**
 *
 */
public enum RangeType {
    DAY,
    MONTH,
    YEAR
}
