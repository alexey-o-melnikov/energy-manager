package com.tiktak.energymanager.prediction;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.RecommendedItem;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.List;

/**
 *
 */
@Slf4j
@RequiredArgsConstructor
@Service
public class Predication {
    @EventListener
    public void predict(ContextRefreshedEvent e) throws Exception {
        DataModel model = new FileDataModel(new File("counters_data.csv"));
        UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
        UserNeighborhood neighborhood = new NearestNUserNeighborhood(2, similarity, model);
        Recommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);
        List<RecommendedItem> recommendations = recommender.recommend(2, 1);
        for (RecommendedItem recommendation : recommendations) {
            log.info(recommendation.toString());
        }

        log.info("over");
    }

//    public static void main(String[] args) {
//        try {
//            DataModel model = new FileDataModel(new File("src/main/java/com/tiktak/energymanager/counters_data.csv"));
//            UserSimilarity similarity = new PearsonCorrelationSimilarity(model);
//            UserNeighborhood neighborhood = new NearestNUserNeighborhood(2, similarity, model);
//            Recommender recommender = new GenericUserBasedRecommender(model, neighborhood, similarity);
//            List<RecommendedItem> recommendations = recommender.recommend(2, 1);
//            for (RecommendedItem recommendation : recommendations) {
//                log.info(recommendation.toString());
//            }
//
//            log.info("over");
//        } catch (Exception e) {
//            e.toString();
//        }
//    }
}
