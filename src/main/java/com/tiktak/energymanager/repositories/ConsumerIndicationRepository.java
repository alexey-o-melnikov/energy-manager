package com.tiktak.energymanager.repositories;

import com.tiktak.energymanager.model.ConsumerIndication;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsumerIndicationRepository extends JpaRepository<ConsumerIndication, Long> {
}
