package com.tiktak.energymanager.model;

import lombok.Data;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@Entity
@Table(name = "consumers")
public class ConsumerIndication {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private long lastUpdateTimestamp;
    @Column
    private LocalDate date;
    @Column
    private int hour;
    @Column
    private long powerByHour;
    @Column
    private long amperage;
    @Column
    private String name;
    @Column
    private String descripton;
    @OneToMany
    private Set<ConsumerIndication> consumers;
    @Column
    @Enumerated(EnumType.STRING)
    private CosumerTypes type;
}
