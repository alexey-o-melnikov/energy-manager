package com.tiktak.energymanager.mappers.sonoff;

import com.tiktak.energymanager.mappers.Mapper;
import com.tiktak.energymanager.model.ConsumerIndication;

/**
 *
 */
public class SonoffMapper implements Mapper<SonoffData> {
    @Override
    public ConsumerIndication inConsumerIndication(SonoffData data) {
        return null;
    }

    @Override
    public SonoffData fromConsumerIndication(ConsumerIndication consumerIndication) {
        return null;
    }
}
