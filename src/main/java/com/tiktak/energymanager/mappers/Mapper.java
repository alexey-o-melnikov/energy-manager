package com.tiktak.energymanager.mappers;

import com.tiktak.energymanager.model.ConsumerIndication;

/**
 *
 */
public interface Mapper<T> {
    ConsumerIndication inConsumerIndication(T data);

    T fromConsumerIndication(ConsumerIndication consumerIndication);
}
